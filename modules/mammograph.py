'''
Created on Dec 14, 2011

@author: Tomas Lipovsky
@author: Tomas Ondrouch
'''
import math
import random

class Mammograph(object):
    '''
    classdocs
    '''
    
    def getPsiz(self):
        result = 0
        if self.dtum >0:
            if self.dtum >= 8.51362 and self.dtum <= 26.4864:
                result = 1
            else:
                expo = math.exp((-1/98.0)*(-17.5+self.dtum)**2)
                result =  (expo/(7*math.sqrt(2*math.pi)))*40
            
        return result
    
    def getPhd(self):
        if self.ph == 1:
            # nador je invazivni
            if self.pd == 0:
            # EF
                return 1.0
            elif self.pd == 1:
            # SF
                return 0.89
            elif self.pd == 2:
            # HD
                return 0.86
            elif self.pd == 3:
            # ED
                return 0.6       
        else:
            # nador je in situ
            if self.pd == 0:
            # EF
                return 1.0
            elif self.pd == 1:
            # SF
                return 0.6
            elif self.pd == 2:
            # HD
                return 0.64
            elif self.pd == 3:
            # ED
                return 0.25  
        

    def getProbOfTumorDetection(self):
        #print "\t Psiz: %f " % self.getPsiz()
        randomNumb = random.uniform(0.0, 1.0)
        pdet = self.getPsiz() * self.getPhd()

        if randomNumb < pdet:
            return 1
        else:
            return 0
    
    def setParams(self, dtum, ph, pd):
        self.dtum = dtum
        self.ph = ph
        self.pd = pd

    def __init__(self, dtum, ph, pd):
        '''
        Constructor
        '''
        self.dtum = dtum
        self.ph = ph
        self.pd = pd
        