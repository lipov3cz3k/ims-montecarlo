'''
Created on Dec 14, 2011

@author: Tomas Lipovsky
@author: Tomas Ondrouch
'''
import random
import math

def enum(**enums):
    return type('Enum', (), enums)

breastDensityType = enum(EF=0, SF=1, HD=2, ED=3)    


class Woman(object):
    '''
    classdocs
    '''
    
    def getHistologicalTypeChange(self):
        '''
        Vrati typ karcinomu - 0 - in situ, 1 - invazivni
        '''
        randomNumb = random.uniform(1, 100)
        result = self.histologicalType
        if result == 0 and self.tumorSize > 0:
            if self.tumorSize == 0.0:
                result = 0
            elif self.tumorSize <=6.0:
                if randomNumb <= 0.0:
                    result = 1
            elif self.tumorSize <=11.0:
                if randomNumb <= 17.0:
                    result = 1
            elif self.tumorSize <= 16.0:
                if randomNumb <= 70.0:
                    result = 1
            elif self.tumorSize <= 20.0:
                if randomNumb <= 67.0:
                    result = 1
            elif self.tumorSize > 20.0:
                if randomNumb <= 80:
                    result = 1           
        return result

    def getHistologicalType(self):
        '''
        Vrati typ karcinomu - 0 - in situ, 1 - invazivni
        '''
        randomNumb = random.uniform(1, 100)
        result = 0
        if self.tumorSize > 0:
            if self.tumorSize == 0.0:
                result = 0
            elif self.tumorSize <=6.0:
                if randomNumb <= 40.0:
                    result = 1
            elif self.tumorSize <=11.0:
                if randomNumb <= 50.0:
                    result = 1
            elif self.tumorSize <= 16.0:
                if randomNumb <= 85.0:
                    result = 1
            elif self.tumorSize <= 20.0:
                if randomNumb <= 95.0:
                    result = 1
            elif self.tumorSize > 20.0:
                if randomNumb <= 99:
                    result = 1           
        return result
    
    
    def getWhenCancerStart(self, startCancerDistribution):
        while(1):
            randomAge = random.randint(30, 90)
            randomCount = random.randint(0, 350)
            randomYearPart = random.uniform(0, 1)
            
            if randomCount <= startCancerDistribution[min(randomAge, 85)]:
                return randomAge + randomYearPart
    
    
    def willHaveBreastCancer(self):
        randomNum = random.uniform(0, 100)
        if(randomNum > 12.0):
            return 0
        else:
            return 1
    
    def getRandomAge(self, ageDistribution):
        while(1):
            randomAge = random.randint(30, 90)
            randomCount = random.randint(0, 100000)
        
            if randomCount <= ageDistribution[randomAge]:      
                return randomAge     
            
    def getTumorVolume(self):
        result = 0
        b = 0.0032 * 365
        t = self.age - self.startCancerAge
        if t >= 0:
            result = 1100000*pow( (1+(1023/math.exp((b*t)/4))), -4)
            result = pow((result * 6/math.pi), 1/3.0)
            
        return result
        
    def probBreastDensity(self):
        '''
        @brief funkce vrati hustotu prsni tkane podle statistickeho rozlozeni, vracene cislo odpovida vyctu breastDensityType
        '''
        breastDensityTable = {}
        breastDensityTable[30] = [0.0, 15.0, 60.0, 25.0]
        breastDensityTable[40] = [2.5, 18.0, 59.0, 20.5]
        breastDensityTable[50] = [0.0, 46.0, 48.6, 5.4]
        breastDensityTable[60] = [7.0, 51.2, 39.5, 2.3]
        breastDensityTable[70] = [12.9, 51.6, 35.5, 0.0]
        
        ageRange = (self.age/10) * 10
        if ageRange > 70:
            ageRange = 70
        if not ageRange in (30, 40, 50, 60, 70):
            print("NEMAM DATA PRO ZADANY VEK")
            
        randomNumb = random.uniform(0.0, 100.0)
        valueSum = 0    
        i = 0
        for i in range(4):
            valueSum += breastDensityTable[ageRange][i]           
            if randomNumb <= valueSum:
                return i
    
    def breastDensityChange(self, decade):
            #print "mozna zmena hustoty prsni tkane - prechod pres %d let" % decade
            randomNumb = random.uniform(0.0, 100.0)
            
            if(self.breastDensity == breastDensityType.ED):
                # ED -> HD
                if (decade == 40 and randomNumb <= 18.0) or (decade == 50 and randomNumb <= 73.7):
                    self.breastDensity = breastDensityType.HD
                elif (decade == 60 and randomNumb <= 57.4) or (decade == 70):
                    self.breastDensity = breastDensityType.HD
            elif(self.breastDensity == breastDensityType.HD):
                # HD -> SF
                if (decade == 40 and randomNumb <= 9.2) or (decade == 50 and randomNumb <= 43.2):
                    self.breastDensity = breastDensityType.SF
                elif (decade == 60 and randomNumb <= 25.1) or (decade == 70 and randomNumb <= 16.0):
                    self.breastDensity = breastDensityType.SF
            elif(self.breastDensity == breastDensityType.SF):
                # SF -> EF
                if (decade == 40 and randomNumb <= 16.7):
                    self.breastDensity = breastDensityType.SF
                elif (decade == 60 and randomNumb <= 15.2) or (decade == 70 and randomNumb <= 11.5):
                    self.breastDensity = breastDensityType.SF


            
    def testForRandomAge(self, ageDistribution):    
        distribution = [0] * 100
        i = 0
        while(i <= 1000000):
            i += 1
            distribution[self.getRandomAge(ageDistribution)] += 1
        
        print "done"
        
    def testForRandomHaveCancer(self):
        distribution = [0] * 2
        i = 0
        while(i <= 1000000):
            i += 1
            distribution[self.willHaveBreastCancer()] += 1
        
        print "done"
        
    def testForRandomStartCancer(self, startCancerDistribution):
        distribution = [0] * 100
        i = 0
        while(i <= 1000000):
            i += 1
            distribution[self.getWhenCancerStart(startCancerDistribution)] += 1
        
        print "done"
        
    def increaseAge(self, years):

        if int((self.age + years) / 10) - int(self.age / 10):
            # prechod pres desitku -> meni se hustota prsni tkane
            self.breastDensityChange(int( ((self.age + years) / 10)*10 ))
        self.age += years
        if self.haveCancer:
            self.histologicalType = self.getHistologicalTypeChange()
        
    def __init__(self, ageDistribution, startCancerDistribution):
        '''
        Constructor
        '''
        #self.testForRandomAge(ageDistribution)
        #self.testForRandomHaveCancer()
        #self.testForRandomStartCancer(startCancerDistribution)
        self.haveCancer = self.willHaveBreastCancer()
        self.age = self.getRandomAge(ageDistribution)
        self.breastDensity = self.probBreastDensity()
        self.histologicalType = 0
        self.tumorSize = 0.0
        self.startCancerAge = 0.0
        if self.haveCancer:
            self.startCancerAge = self.getWhenCancerStart(startCancerDistribution)
            self.tumorSize = self.getTumorVolume()
            self.histologicalType = self.getHistologicalType()
            
    def printPersonInfo(self):
        print("Dostane rakovinu: %d " % self.startCancerAge)
        
    def printInfo(self):
        print("Vek: %d" % self.age)
        print("Hustota prsni tkane: %d" % self.breastDensity)
        if self.haveCancer:
            if self.tumorSize:
                print("Velikost nadoru %f mm"% self.tumorSize)
                print("nador roste jiz celych %f"% (self.age - self.startCancerAge))
            else:
                print("Tumor se jeste nevytvoril")
            print("Karcinom prsu: %d " % self.histologicalType)
            
        