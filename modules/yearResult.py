'''
Created on Dec 17, 2011

@author: Lipov3cz3k
'''

class YearResult(object):
    def __init__(self):
        self.mamo = 0 # pocet detekovanych mamografem
        self.toOld = 0 # pocet ktery musel opustit program kvuli stari
        self.total = 0 # celkovy pocet testovanych pacientu v roce
        self.inSitu = 0 # pocet pacientu s in situ

    def incTotal(self):
        self.total += 1
        
    def incToOld(self):
        self.toOld += 1
    
    def incMamo(self):
        self.mamo += 1
        
    def incInSitu(self):
        self.inSitu += 1