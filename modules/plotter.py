'''
Created on Dec 17, 2011

@author: Lipov3cz3k
'''
from yearResult import YearResult


class plotter(object):
    '''
    classdocs
    '''

    def genPlotCfgBreastCancerProgram(self,type , outDir, outFile, screeningFrom, screeningTo, screeningInterval):
        confFile = open("%s%s.cfg" %(outDir, outFile), "w")
        confFile.write("set key horiz outside bot center\n\
                        set xtics 1\n\
                        set xtics in\n\
                        set xlabel \"Runtime [years]\"\n\
                        set grid back\n\
                        set terminal wxt size 1200,600\n")
        
        if type == "bcp":
            confFile.write("set ylabel \"Detection rate [per thousands]\"\n\
                        set title \"Breast cancer screening program [%d, %d]\"\n\
                        plot [-1:30][0:10] \\\n" %(screeningFrom,screeningTo))
        elif type == "detected":
            confFile.write("set ylabel \"Detected tumors [persons]\"\n\
                        set title \"Breast cancer screening program [%d, %d]\"\n\
                        plot [-1:30][0:*] \\\n" %(screeningFrom,screeningTo))
                        
        if type == "bcp":
            confFile.write("\"%s.dat\" title 'Detected by mamo in interval %i' with linespoints \n" %(self.outFile, screeningInterval))
        elif type == "detected":
            confFile.write("\"%s.dat\" title 'Detected by mamo in interval %i' with linespoints \n" %(self.outFile, screeningInterval))
        confFile.write("pause -1 \"Press return to continue...\"\n")    
        confFile.close()



    def genPlotDetectionChart(self, totalPersons, screeningFrom, screeningTo, screeningInterval):
        self.outDir = "./plot/"
        self.outFile = "detection_%d_%dint"%(totalPersons, screeningInterval)
        
        out = open("%s%s.dat" %(self.outDir, self.outFile), "w")
        i = 0 
        for year in self.results:
            out.write("%d \t%d \n" %(i, year.total))
            i += 1
        out.close()
        
        self.genPlotCfgBreastCancerProgram("detected", self.outDir, self.outFile, screeningFrom, screeningTo, screeningInterval)
        
    def genPlotBreastCancerProgram(self, totalPersons, screeningFrom, screeningTo, screeningInterval):
        self.outDir = "./plot/"
        self.outFile = "output_%d_%dint"%(totalPersons, screeningInterval)
        
        out = open("%s%s.dat" %(self.outDir, self.outFile), "w")
        i = 0 
        for year in self.results:
            if year.total > 0:
                out.write("%d \t%f\n" % (i, float(year.mamo)/year.total *1000 ))
            else:
                out.write("%d \t0\n" % (i))
            i += 1
        out.close()
        
        self.genPlotCfgBreastCancerProgram("bcp", self.outDir, self.outFile, screeningFrom, screeningTo, screeningInterval)
        

    def __init__(self, results):
        '''
        Constructor
        '''
        self.results = results