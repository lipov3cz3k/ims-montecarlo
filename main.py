'''
Created on Dec 14, 2011

@author: Tomas Lipovsky
@author: Tomas Ondrouch
'''
import sys
from ImageChops import screen



sys.path+="./modules/"

import getopt
import math
import random
from woman import Woman
from mammograph import Mammograph
from yearResult import YearResult
from plotter import plotter


ageDistributionFilename = "VekoveRozlozeni.csv"
startCancerDistributionFilename = "VekoveRozlozeniVyskytuRakoviny.csv"

ageDistribution = list()
startCancerDistribution = list()

# nacte vekove rozlozeni zen
ifile = open(ageDistributionFilename, "r")
for row in ifile:
    ageDistribution.append(int(row))
ifile.close()

# nacte rozlozeni objeveni rakoviny podle veku
ifile = open(startCancerDistributionFilename, "r")
for row in ifile:
    if not startCancerDistribution:
        startCancerDistribution.append(float(row))
    else:
        first = startCancerDistribution[-1]
        last = float(row)
        step = (last - first) / 5
        for i in range(1, 6):
            startCancerDistribution.append(first + i * step)
        
ifile.close()

# STATISTICS
totalPersons = 0
detectedScreenInvasive = 0
detectedScreenInSitu = 0
totalScreenTumorSize = 0
detectedScreenHaveCancer = 0

totalClinicTumorSize = 0
detectedClinicHaveCancer = 0
detectedClinicInSitu = 0
detectedClinicInvasive = 0


def usage():
    print ("How to use simulator")
    print("-f --from=\t\tAge, when screening program begin")
    print("-t --to=\t\tAge, when screening program ends")
    print("-i --interval=\t\tInterval of screenings in years")
    
def probOfClinicalDetection(dtum):
    randomNumb = random.uniform(0.0, 1.0)    
    prob = 0
    if dtum >= 15:
        prob = (1/20) * math.exp(-15/20)
        if randomNumb >= prob:
            prob = 1
    return prob
    
def screeningProgram(screeningFrom, screeningTo, screeningInterval, totalPersons):
    global detectedScreenInvasive, detectedClinicInvasive, detectedScreenInSitu, detectedClinicInSitu, detectedScreenHaveCancer, detectedClinicHaveCancer
    global totalScreenTumorSize, totalClinicTumorSize
    
    screeningResults = []
    for i in xrange(screeningTo):
        screeningResults.append(YearResult())
        
    device = Mammograph(0, 0, 0)
    
    for i in xrange(totalPersons):
        if not i % 10000:
            print ("%d/%d"%(i,totalPersons))
        person = Woman(ageDistribution, startCancerDistribution)
        

        if person.age > screeningTo:
            #print("NEVSTOUPI DO PROGRAMU, ale zaznamena se to nekam - je moc stara")
            pass
        elif person.haveCancer == 1 and probOfClinicalDetection(person.tumorSize):
            # pokud zena ma rakovinu pred zacatkem programu a objevi ji, nevstupuje do programu
            #print("NEVSTOUPI DO PROGRAMU - jiz rakovinu ma")
            
            pass
            
        else:
            startYearOfProgram = 0
            if person.age >= screeningFrom:
                startYearOfProgram = 0
            else:
                startYearOfProgram = screeningFrom - person.age
                person.increaseAge(startYearOfProgram) # pocka nez bude moct vstoupit do programu
                
            roundCounter = 0
            device.setParams(person.tumorSize, person.histologicalType, person.breastDensity)
            
            endingType = 0 # 0 - nenalezen; 1 - nalezen; 2 - ukonceno kvuli veku
            while endingType == 0:
                
                screeningResults[startYearOfProgram + roundCounter*screeningInterval].incTotal()
                if person.histologicalType == 0:
                    screeningResults[startYearOfProgram + roundCounter*screeningInterval].incInSitu()
                
                if device.getProbOfTumorDetection() == 1 and person.haveCancer == 1:
                    #print("tumor nalezen")
                    endingType = 1
                    screeningResults[startYearOfProgram + roundCounter*screeningInterval].incMamo()
                    
                    #statistics
                    if person.histologicalType == 0:
                        detectedScreenInSitu +=1
                    else:
                        detectedScreenInvasive +=1
                        totalScreenTumorSize += person.tumorSize
                elif person.age > screeningTo:
                    #print("zena konci v programu kvuli stari")
                    endingType = 2
                    screeningResults[startYearOfProgram + roundCounter*screeningInterval].incToOld()
                             
                if endingType == 0:
                    person.increaseAge(screeningInterval)
                    if probOfClinicalDetection(person.tumorSize) == 1 and person.haveCancer == 1:
                        #print("Tumor nalezen mezi pravidelnymi prohlidkami")
                        endingType = 1
                        screeningResults[startYearOfProgram + roundCounter*screeningInterval].incMamo()
                        
                        #statistics
                        if person.histologicalType == 0:
                            detectedScreenInSitu +=1
                        else:
                            detectedScreenInvasive +=1
                            totalScreenTumorSize += person.tumorSize
                    else:
                        device.setParams(person.tumorSize, person.histologicalType, person.breastDensity)
                    roundCounter += 1  
                
    
    return screeningResults


totalPersons = 0
screeningFrom = -1
screeningTo = -1
screeningInterval = 0
# ziskani parametru
try:
    opts, args = getopt.getopt(sys.argv[1:], "ht:s:e:i:", ["help", "total=","start=","end=", "interval="])
except getopt.GetoptError, err:
    # print help information and exit:
    print str(err) # will print something like "option -a not recognized"
    usage()
    sys.exit(2)
output = None
verbose = False
if len(sys.argv) <=1:
    print("Missing parameters")
    usage()
    sys.exit(2)
for o, a in opts:
    if o == "-v":
        verbose = True
    elif o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-t", "--total"):
        if int(a) > 0:
            totalPersons = int(a)
        else:
            print("Error: too few persons for simulate!")
            sys.exit()
    elif o in ("-s", "--start"):
        if int(a) > 0:
            screeningFrom = int(a)
        else:
            print("Error: Starting age must be positive")
            sys.exit() 
    elif o in ("-e", "--end"):
        if int(a) > 0:
            screeningTo = int(a)
        else:
            print("Error: Ending age must be positive")
            sys.exit() 
    elif o in ("-i", "--interval"):
        if int(a) > 0:
            screeningInterval = int(a)
        else:
            print("Error: Screening interval must be positive")
            sys.exit()
    else:
        assert False, "unhandled option"


if screeningFrom > screeningTo or screeningFrom < 0 or screeningTo < 0:
    print("Error in screening range")
    usage()
    sys.exit(2)
if screeningInterval <=0:
    print("Error: Screening interval must be positive")
    sys.exit()
if totalPersons <=0:
    print("Error: too few persons for simulate!")
    sys.exit()

print("total persons: %d" %totalPersons)
print("from: %d" %screeningFrom)
print("to: %d" %screeningTo)
print("inter: %d" %screeningInterval)

print("--==SIMULATING==--")
results = screeningProgram(screeningFrom, screeningTo, screeningInterval, totalPersons)

print("--==DONE==--")
plot = plotter(results)
plot.genPlotBreastCancerProgram(totalPersons, screeningFrom, screeningTo, screeningInterval)
plot.genPlotDetectionChart(totalPersons, screeningFrom, screeningTo, screeningInterval)


print("--====STATISTICS====--")
print("Total persons:\t\t\t%d" %totalPersons)
print("Detected screen invasive:\t%d" %detectedScreenInvasive)
print("Detected screen in situ:\t%d" %detectedScreenInSitu)
print("Screen avg. tumor size:\t\t%f" %(totalScreenTumorSize/detectedScreenInvasive))